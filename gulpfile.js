// For development => gulp
// For production  => gulp -p

// Call Plugins
var env         = require('minimist')(process.argv.slice(2)),
    gulp        = require('gulp'),
    rename      = require('gulp-rename'),
    gutil       = require('gulp-util'),
    plumber     = require('gulp-plumber'),
    nunjucks    = require('gulp-nunjucks-html'),
    minifyHtml  = require('gulp-minify-html'),
    htmltidy    = require('gulp-htmltidy'),
    browserSync = require('browser-sync'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    sass        = require('gulp-sass'),
    gulpif      = require('gulp-if'),
    cache       = require('gulp-cache'),
    newer       = require('gulp-newer'),
    imagemin    = require('gulp-imagemin'),
    del         = require('del')

// Compile Nunjucks Templates

gulp.task('nunjucks', function(){
    return gulp.src('src/pages/*.njk')
        .pipe(plumber())
        .pipe(nunjucks({
            searchPaths: ['src/pages']
        }))
        .pipe(rename({
          extname: '.html'
        }))
        .pipe(htmltidy({
          doctype: 'html5',
          hideComments: true,
          indent: true
        }))
        .pipe(gulpif(env.p, minifyHtml()))
        .pipe(gulp.dest('build/'));
});

// Uglify and Concat JS

gulp.task('js', function(){
    return gulp.src('src/js/**/*.js')
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulpif(env.p, uglify()))
        .pipe(gulp.dest('build/js'));
});

gulp.task('styles', function() {
    gulp.src('src/sass/**/*.scss')
        .pipe(plumber())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('build/css'));
});

// Optimize images
// Without "Newer" all images are copied over every time a new images is added

gulp.task('imagemin', function() {
    return gulp.src('src/img/**/*')
        .pipe(plumber())
        .pipe(newer('build/img'))
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
        .pipe(gulp.dest('build/img'));
});

// Copy fonts

gulp.task('fonts', function() {
  return gulp.src('src/fonts/**/*')
  .pipe(gulp.dest('build/fonts'))
})

// Watch folders (not including fonts)

gulp.task('watch', function(){
    gulp.watch('src/pages/**/*.njk', ['nunjucks']);
    gulp.watch('src/sass/**/*.scss', ['styles']);
    gulp.watch('src/js/**/*.js', ['js']);
    gulp.watch('src/img/**/*.{jpg,png,gif,svg}', ['imagemin']);
});

// Tell browser-sync to watch these files in the build directory

gulp.task('browser-sync', function () {
   var files = [
      'build/**/*.html',
      'build/css/**/*.css',
      'build/img/**/*',
      'build/js/**/*.js'
   ];

   browserSync.init(files, {
      server: {
         baseDir: './build/'
      },
      port : 3200
   });
});

// Deletes the build directory, (not included in default tasks)

gulp.task('clean:build', function() {
  return del.sync('build');
})

// Default task
gulp.task('default', ['nunjucks', 'js', 'styles', 'imagemin', 'fonts', 'watch', 'browser-sync']);
