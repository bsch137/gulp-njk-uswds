#Build file for the USWDS framework and Nunjucks templates

---

A gulp build file with a boilerplate template using the framework by USWDS
(U.S. Web Design Standards). It compiles Nunjucks templates into static html;
imports the USWDS Sass files from npm; concatenates and uglifies js; compresses
image files, and copies the fonts over to the build directory (fonts are from 
the USWDS git repository).

More help on these great tools:
- [GulpJS](http://gulpjs.com/)
- [Nunjucks](https://mozilla.github.io/nunjucks/)
- [USWDS](https://github.com/18F/web-design-standards/)
- [Sass](http://sass-lang.com/guide/)
- [Browser-sync](http://www.browsersync.io/)


## Getting Started

### Installation

First of all, install the dependencies to run this boilerplate.

- [NodeJS](http://nodejs.org/)
- [GulpJS](http://gulpjs.com/)


```sh
# Clone this repository
$ git clone git@bitbucket.org:barplatt/gulp-njk-uswds.git new_project
$ cd new_project

# Install dependencies
$ npm install

# Start local server
$ gulp
```

With the commands above, you have everything to start.

### Tasks

- `gulp`: Initialize watch for changes and start server at http://localhost:3000
- `gulp js`: execute js files
- `gulp nunjucks`: compile nunjucks templates
- `gulp styles`: compile sass files
- `gulp imagemin`:compress image files
- `gulp watch`: call for watch files
- `gulp clean:build`: deletes the build directory
- `gulp -p`: minify all files for production

### Default Folders and Files

```sh
new_project -
    /build -
        /css
            main.css
        /fonts
        /img
        /js
            main.js
    /src -
        /fonts
        /img
        /js
        /pages
            /templates
        /sass

        .gitignore
        gulpfile.js
        package.json
        readme.md
```

### License

MIT License

Copyright (c) 2017 Benny Schalin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.